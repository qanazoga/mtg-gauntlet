# The Basics
This is an attempt to mix things up for my MTG group.

## Inspiration
This projects takes inspiration from [Slay the Spire](https://store.steampowered.com/app/646570/Slay_the_Spire/) (A rougelike deckbuilding card game that I highly recommend), 
and (possibly) [Betrayal at the House on the Hill](https://avalonhill.wizards.com/avalon-hill-betrayal-house-hill) (another game by Wizards of the Coast, and one I again recommend!)

## Rundown
One or more players play as though they are on Theros, 
and will play against each of the [Challenge Decks](https://mtg.fandom.com/wiki/Challenge_Deck) from the block, 
they may take time between fights to get random bonuses or pitfalls.
(I haven't decided exactly how this works yet tho.)

Once they are finished, one of few possible things could happen (I haven't decided yet)
1. one player betrays the rest (chosen at random(?)), they get a preconstructed [Archenemy](https://mtg.fandom.com/wiki/Archenemy_(format)) deck, then it is a battle to see who wins.
   - Players may provide their own Archenemy deck, if they don't, there should be a default one
2. it's a free for all, players will not share a life total during the boss fights, but will share damage.
3. all bosses at once I dunno.


There will probably also be a banlist specific to this format.  
Yes, it's an extremely nerdy project, but I think it'll be fun :)
