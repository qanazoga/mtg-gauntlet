# Default Player Decks
I am lazy, so I'm just picking decks of Planeswalkers who have been to Theros.
Alternatively, I already have a Planechase set and I think those decks are fun so maybe just those.

## Decks:
- Duel Decks: Elspeth vs Kiora - Both are Theros staples
- Ajani has a couple.
  - Ajani Core 2020
  - Ajani AEther revolt
  - Duel Decks: Ajani vs Nicol Bolas - Bolas wasn't cannonically there but you know Bolas, gotta fuck w/ my cannon
- Ashiok: Sculptor of Fears deck

Alternatively, you could get a couple Archenemy decks, then use the scheme decks at the end (if going the betrayal route) for a big baddie beat down.

