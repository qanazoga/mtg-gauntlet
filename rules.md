# Rules
## Version 0.1
These *might* vary slightly from the official rules for each individual fight.
This is being optimized for Modern decks. Your experience my differ with other formats.

## Basics
### Heroes
- At the start of the adventure, each player is allowed their choice of 3 randomly selected heroes.
- after each battle each player gets to select their choice of 1 of 2 randomly selected heroes
- during each battle the total number of heroes is for *ALL* players, not each individual player.
- during the final battle each player may use all Heroes they control.

### Dungeons
This doesn't use the normal rules for Dungeons, and cards using the phrase "Venture into the Dungeon" are not required to play.
- Each battle has a selected dungeon.
  - players may only move through the dungeon for as long as they are in that dungeons battle
- Each player may pay {3} at any time they could cast a sorccery to Venture into the Dungeon. Each player may only do this once per turn.
  - cards that do have the "Venture into the Dungeon" keyword still allow for progression.
    - This will work even after the player has used their ability to venture.
  - if they are outside the dungeon this will put them in the first room
  - if they are inside the dungeon they may move to one of the connected next rooms.
  - players may not move backwards through the dungeon
- players move through the dungeon seperately
- the first player to venture through *and leave* the dungeon gets their choice of one of 3 randomly selected Vanguards.
- if all players fail to make it through the dungeon, each recieves a random curse.
  - if even a single player makes it through, the above effect is negated.
   
### Vanguards
- Each player may only ever have 1 Vanguard.
- If a player already has a Vanguard and is the first player to complete a dungeon, they do not get a new choice, they only remove the choice for following players.

## Face the Hydra
### Setup
- The Dungeon for this battle is Lost Mine of Phandelver.
- Choose a starting number of Heads. Take that many cards named Hydra Head from the Challenge Deck and place them on the battlefield. Shuffle the remaining cards to form The Hydra's library.
- The Players start with up to two Hero cards on the battlefield. (The Players don't need any Heroes to play.)
  - There is no maximum number of Heads
  - The number of starting heads for correlating difficulty is listed below.
  - Easy - 2 Heads.
  - Medium - 3 Heads.
  - Hard - 4 Heads.
### Player turn
- The Players go first. (The Players don't draw a card on first turn.)
- The Players proceed with play as if playing a regular game. 
    
### Hydra turn
- At the start of The Hydra's turn, untap any tapped Heads.
- Reveal the top card of The Hydra's library. The Hydra casts that card. When the spell resolves, if it's a Head, put it onto the battlefield. If it's a sorcery, follow its instructions and then put it into The Hydra's graveyard.
- The Hydra deals 1 damage to the players for each untapped card named Hydra Head it controls and 2 damage to The Players for each untapped Elite Head it controls.
  - Any Heads that list the amount of damage they deal is in addition to the above numbers.
      
### Additional rules
- You may attack Heads directly with your creatures.
  - any number of creatures may attack a single Head.
- The Hydra does not have a life total.
  - If The Hydra would lose life, instead deal that much damage to a Head of your choice.
    - This is a replacement effect, Deathtouch does not affect players, so replaced Deathtouch damage will not immedietly kill heads
- Whenever a Head leaves the battlefield, reveal the top two cards of The Hydra's library. The Hydra casts any Heads, and any sorcery cards into The Hydra's graveyard.
- Ignore effects that would cause The Hydra to draw or discard cards, or any impossible actions.
  - Since Milling does not draw cards, The Hydra *can* be milled. Any variations of mill that involve drawing cards are still impossible.
- If a Head would move to any other zone than the graveyard, instead put it into The Hydra's graveyard.
- The Players make any choices The Hydra needs to make.
  - It is recommended that you make the choice most beneficial for The Hydra.
- When The Hydra has no Heads it is defeated, and The Players win.
  - The Hydra only loses if it has no Heads, milling can not kill The Hydra directly.
      
## Battle the Horde
### Setup
- The Dungeon for this battle is Dungeon of the Mad Mage.
- The Players start with up to three Hero cards on the battlefield. (The Players don't need any Heroes to play.)

### Player turn
- The Players take 3 turns before The Horde takes its first turn.
- The Players proceed with play as if playing a regular game. 

### Horde turn
- At the beginning of The Horde's precombat main phase each turn, reveal the top two cards of The Horde's library, then The Horde casts those cards.
  - The Horde's artifacts each cause an additional card to be revealed and cast, starting the turn after the artifact enters the battlefield.
  - The Horde flips and casts an additional card per turn for each additional player
  
### Additional rules
- Unlike the prior and next battle, The Players may not attack The Horde's creatures.
- The Horde does not have a life total
  - If The Horde would lose life, instead mill that amount of cards from its library
  - The Players may attack The Horde or target it with spells before its first turn.
- Ignore effects that would cause The Horde to draw or discard cards, or any impossible actions.
  - Since Milling does not draw cards, The Horde *can* be milled. Any variations of mill that involve drawing cards are still impossible.
- If one of The Horde's permanents would move to any other zone than the graveyard, instead put it into The Horde's graveyard.
- The Players make any choices The Horde needs to make.
  - It is recommended that you make the choice most beneficial for The Horde.
- When The Horde has no cards left in its library and controls no creatures it is defeated, and The Players win.
    
## Defeat a God

### Setup
- The Dungeon of this battle is Tomb of Annihilation.
- The Players start with up to three different Hero cards on the battlefield. (The Players don't need any Heroes to play.)
- Xenagos Ascended and two Rollicking Throng begin the game on the Battlefield.
    
### Player turn
- The Players may attack Xenagos Ascended and the Revellers directly with your creatures
  - Any number of creatures can attack a single legal target.
- Besides the above, ThePlayers play as if playing a regular game.

### Xenagos' turn
- At the beginning of Xenagos' main phase each turn, reveal the top two cards of the Xenagos's library. Then Xenagos casts those cards.
  - Flip and cast an additional card for each player beyond the first.
- Xenagos Ascended and reveler creatures don't attack unless the card specifically says they do.
- You make the choice if Xenagos needs to make a decision.

### Aditional rules
- Xenagos (The "Player") does not have a life total.
  - If The Xenagos would lose life, instead deal that much damage to any creature it controls.
    - This is a replacement effect, Deathtouch does not affect players, so replaced Deathtouch damage will not immedietly kill heads.
- Ignore effects that would cause Xenagos to draw or discard cards, or perform any other impossible actions.
- If one of Xenagos's pemanents would go to any zone other than his library or graveyard, that card is put into Xenagos' graveyard.
    
## Final Battle
If more than one player is playing they may face off against each other.
For every mode each player may use all the resources they have collected.
- Free for all.
   - if players finish all battles with roughly the same health and amount of assets, use this option.
- Teams.
   - an alternative to the conditions listed above, or if 2 players have a higher amount of resources/health and 2 have less, use this option
- 1 v Rest.
   - if a single player is dramatically more powerful than the rest, this may be a good option.
   - the inclusion of an archenemy deck could make this even more interesting.
   
# Additional rules.
- The Players start at 30 life, but do not share health.
  - Some effects (Vanguards, Heroes, cards in decks, etc) may change this.
    - Players do not share starting health.
  - damage from enemies (Hydras, Minitaurs, Revelers, or spells thereof) effect each players health unless otherwise noted.
- For additional challenge, player health does not reset between battles.
