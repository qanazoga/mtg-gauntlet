# Heroes
These are kinda weak tbh, but they're ok, plus they were part of the original challenge deck so we may as well include here.
Probably not gonna do the dynamic Godslayers tho.
<details>
<summary> Heroes (images) </summary>
    <img alt="The Protector" src="https://c1.scryfall.com/file/scryfall-cards/large/front/c/a/ca66cd86-1574-4e6e-94de-a19d8d26836f.jpg?1561758086" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Philosopher" src="https://c1.scryfall.com/file/scryfall-cards/large/front/e/f/efbffd56-92a1-4b17-8d53-e9f28b1872e1.jpg?1561758389" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Avenger" src="https://c1.scryfall.com/file/scryfall-cards/large/front/d/0/d0f83818-2c47-435c-ade3-302e4eabc2bf.jpg?1561758124" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Warrior" src="https://c1.scryfall.com/file/scryfall-cards/large/front/d/d/dd60768a-c47f-4af5-bb33-c17777c7dd66.jpg?1561758221" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Hunter" src="https://c1.scryfall.com/file/scryfall-cards/large/front/4/e/4ef068ff-934c-4e4d-a4ae-e44f194b8dca.jpg?1561757110" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Harvester" src="https://c1.scryfall.com/file/scryfall-cards/large/front/6/c/6c17db56-68ee-44c5-82c5-9ed9f03c5899.jpg?1561757365" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Slayer" src="https://c1.scryfall.com/file/scryfall-cards/large/front/5/a/5a12f3e3-5708-4d14-a1ed-600232002266.jpg?1561757199" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Savant" src="https://c1.scryfall.com/file/scryfall-cards/large/front/5/1/51726f8f-b63d-4e93-82b2-d63d361a3b70.jpg?1561757135" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Tyrant" src="https://c1.scryfall.com/file/scryfall-cards/large/front/b/e/be5044ab-5f7d-4109-8574-42306ca86b4c.jpg?1561757961" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Warmonger" src="https://c1.scryfall.com/file/scryfall-cards/large/front/f/0/f02b5e79-5eea-4cbe-b087-241cf27f0580.jpg?1561758391" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Provider" src="https://c1.scryfall.com/file/scryfall-cards/large/front/d/9/d9987b43-7f20-46ce-b7dd-f3126e5bc991.jpg?1561758193" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Explorer" src="https://c1.scryfall.com/file/scryfall-cards/large/front/1/9/1936aa23-3a5d-4386-bc4d-66841e0cab2c.jpg?1561756731" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Vanquisher" src="https://c1.scryfall.com/file/scryfall-cards/large/front/0/8/08a63083-997b-4a72-af48-cd35e6e9599d.jpg?1561756617" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Destined" src="https://c1.scryfall.com/file/scryfall-cards/large/front/8/0/80c9f81c-a6d7-4ffd-b2c8-01ef025b272f.jpg?1561757472" style="border-radius: 15px" width="342" height="auto">
    <img alt="The Champion" src="https://c1.scryfall.com/file/scryfall-cards/large/front/9/a/9a6eba2c-a980-4a2b-b103-bd91bc5889d2.jpg?1561757656" style="border-radius: 15px" width="342" height="auto">
</details>

<details>
    <summary> Heroes (table) </summary>
    
| Name            | Ability                                                                                                                                                                 | Level |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----:|
| The Protector   | {T}: Prevent the next 1 damage that would be dealt to any target this turn.                                                                                             | 1     |
| The Philosopher | {2},{T}: Tap target creature.                                                                                                                                           | 1     |
| The Avenger     | {3}, {T}: Target creature you control gains deathtouch until end of turn.                                                                                               | 1     |
| The Warrior     | {T}: Target creature you control gains haste until end of turn.                                                                                                         | 1     |
| The Hunter      | {T}: Target creature you control gets +1/+1 until end of turn.                                                                                                          | 1     |
| The Harvester   | {T}: Draw a card, then discard a card.                                                                                                                                  | 2     |
| The Slayer      | You start the game with an additional 7 life.                                                                                                                           | 3     |
| The General     | Exile The General: Creatures you control get +1/+1 until end of turn. Untap them.                                                                                       | 4     |
| The Savant      | Exile The Savant: Tap all creatures your opponents control. Those creatures don’t untap during their controllers’ next untap steps.                                     | 4     |
| The Tyrant      | Exile The Tyrant: Creatures your opponents control get -1/-1 until end of turn.                                                                                         | 4     |
| The Warmonger   | Exile The Warmonger: Creatures you control get +2/+0 and gain haste until end of turn.                                                                                  | 4     |
| The Provider    | Exile The Provider: Put two +1/+1 counters on target creature you control. You gain life equal to that creature’s toughness.                                            | 4     |
| The Explorer    | You may play an additional land on each of your turns.                                                                                                                  | 5     |
| The Vanquisher  | Your starting hand size is increased by one. Your maximum hand size is increased by one.                                                                                | 6     |
| The Destined    | Spells you control that target a creature you control cost {2} less to cast.                                                                                            | 8     |
| The Champion    | {2}, {T} Exile Champion: Search your library for a legendary artifact card that isn’t a creature, reveal it, and put it onto the battlefield. Then shuffle your library | 9     |

</details>

# Vangaurds
Not 100% sure how these would be implemented and they're also HELLA expensive.
Something to think about tho.

<details>
    <summary> Vanguards (images) </summary>
    <img alt="Ashnod" src="https://c1.scryfall.com/file/scryfall-cards/large/front/0/4/046f2416-c206-4344-aa39-ec42585e0d64.jpg?1562384970" style="border-radius: 15px" width="342" height="auto">
    <img alt="Barrin" src="https://c1.scryfall.com/file/scryfall-cards/large/front/0/2/02f6b17c-89bd-47e1-9963-69260d5f2838.jpg?1562384961" style="border-radius: 15px" width="342" height="auto">
    <img alt="Crovax" src="https://c1.scryfall.com/file/scryfall-cards/large/front/9/9/991f614e-7550-46ca-9091-96167d780c6c.jpg?1562385470" style="border-radius: 15px" width="342" height="auto">
    <img alt="Eladamri" src="https://c1.scryfall.com/file/scryfall-cards/large/front/a/b/ab25accd-93d8-4ba4-8254-e5eb26ebce4c.jpg?1562385495" style="border-radius: 15px" width="342" height="auto">
    <img alt="Ertai" src="https://c1.scryfall.com/file/scryfall-cards/large/front/5/c/5cbb9b5d-9199-4a5b-957d-8fa681caeb7c.jpg?1562385059" style="border-radius: 15px" width="342" height="auto">
    <img alt="Gerrard" src="https://c1.scryfall.com/file/scryfall-cards/large/front/9/f/9f7413f4-0ce3-4fd4-b011-4e30ad88f510.jpg?1562385478" style="border-radius: 15px" width="342" height="auto">
    <img alt="Gix" src="https://c1.scryfall.com/file/scryfall-cards/large/front/8/7/87c1234b-3834-4bba-bef2-05707bb1e8e2.jpg?1562385446" style="border-radius: 15px" width="342" height="auto">
    <img alt="Graven il-Vec"src="https://c1.scryfall.com/file/scryfall-cards/large/front/2/8/286ec938-81a2-4626-b340-b7aa0192a4bd.jpg?1593863838" style="border-radius: 15px" width="342" height="auto">
    <img alt="Hanna" src="https://c1.scryfall.com/file/scryfall-cards/large/front/0/7/079bdaa4-03ab-4eda-bd7f-ca598118fb47.jpg?1562384986" style="border-radius: 15px" width="342" height="auto">
    <img alt="Karn" src="https://c1.scryfall.com/file/scryfall-cards/large/front/8/0/809175d4-115f-4901-9194-766a81a8ccd4.jpg?1562385437" style="border-radius: 15px" width="342" height="auto">
    <img alt="Lyna" src="https://c1.scryfall.com/file/scryfall-cards/large/front/3/3/3309d7fc-6cff-4599-b480-042d3dfa3342.jpg?1562385020" style="border-radius: 15px" width="342" height="auto">
    <img alt="Maraxus" src="https://c1.scryfall.com/file/scryfall-cards/large/front/5/7/5724de5d-0a43-4b83-89cc-2d87a658964a.jpg?1562385051" style="border-radius: 15px" width="342" height="auto">
    <img alt="Mirri" src="https://c1.scryfall.com/file/scryfall-cards/large/front/2/5/25a203e3-2deb-42f6-b777-b153534280a5.jpg?1562385004" style="border-radius: 15px" width="342" height="auto">
    <img alt="Mishra" src="https://c1.scryfall.com/file/scryfall-cards/large/front/0/5/05c8de73-fd56-49fa-aa17-d04ff1847f8d.jpg?1562384978" style="border-radius: 15px" width="342" height="auto">
    <img alt="Multani" src="https://c1.scryfall.com/file/scryfall-cards/large/front/b/1/b1e0bc9f-6a63-4763-a19d-8adbd6434172.jpg?1562385519" style="border-radius: 15px" width="342" height="auto">
    <img alt="Oracle" src="https://c1.scryfall.com/file/scryfall-cards/large/front/a/f/af5cc745-7072-4f7f-94eb-c7670cb498ff.jpg?1562385511" style="border-radius: 15px" width="342" height="auto">
    <img alt="Orim" src="https://c1.scryfall.com/file/scryfall-cards/large/front/b/7/b745121a-4da9-46ef-938f-8562df5d1f27.jpg?1562385535" style="border-radius: 15px" width="342" height="auto">
    <img alt="Rofellos" src="https://c1.scryfall.com/file/scryfall-cards/large/front/4/e/4e485e72-bca0-46d5-b59d-89f17389ec26.jpg?1562385043" style="border-radius: 15px" width="342" height="auto">
    <img alt="Selenia" src="https://c1.scryfall.com/file/scryfall-cards/large/front/5/e/5e776489-3c5c-42a7-9e2b-2e1f377ebaf4.jpg?1562385413" style="border-radius: 15px" width="342" height="auto">
    <img alt="Serra" src="https://c1.scryfall.com/file/scryfall-cards/large/front/c/d/cd55ffd2-cb98-4c66-a79d-0da5e927360c.jpg?1562385551" style="border-radius: 15px" width="342" height="auto">
    <img alt="Sidar Kondo" src="https://c1.scryfall.com/file/scryfall-cards/large/front/3/8/3835b685-6dc7-4e0d-84a8-71495fa73dcd.jpg?1562385027" style="border-radius: 15px" width="342" height="auto">
    <img alt="Sisay" src="https://c1.scryfall.com/file/scryfall-cards/large/front/b/3/b3e1176b-6b7e-481e-ba19-3799ddfad470.jpg?1562385527" style="border-radius: 15px" width="342" height="auto">
    <img alt="Sliver Queen, Brood Mother" src="https://c1.scryfall.com/file/scryfall-cards/large/front/9/0/90e5cab2-b540-4c04-9fb1-be193888f605.jpg?1562385454" style="border-radius: 15px" width="342" height="auto">
    <img alt="Squee" src="https://c1.scryfall.com/file/scryfall-cards/large/front/1/c/1cc16638-093d-4010-89ae-a1e564921e62.jpg?1562384994" style="border-radius: 15px" width="342" height="auto">
    <img alt="Starke" src="https://c1.scryfall.com/file/scryfall-cards/large/front/9/0/90ecb93b-547f-496f-8a4e-bd5a69f8088f.jpg?1562385462" style="border-radius: 15px" width="342" height="auto">
    <img alt="Tahngarth" src="https://c1.scryfall.com/file/scryfall-cards/large/front/a/3/a320e0f4-bcc9-4bfa-8e05-b62f9752c01c.jpg?1562385487" style="border-radius: 15px" width="342" height="auto">
    <img alt="Takara" src="https://c1.scryfall.com/file/scryfall-cards/large/front/7/6/763993dc-edd5-4c1e-a18f-a98cc3ca401d.jpg?1562385429" style="border-radius: 15px" width="342" height="auto">
    <img alt="Tawnos" src="https://c1.scryfall.com/file/scryfall-cards/large/front/5/f/5f92bfac-c4ad-4155-855e-cf330f2c8145.jpg?1593452678" style="border-radius: 15px" width="342" height="auto">
    <img alt="Titania" src="https://c1.scryfall.com/file/scryfall-cards/large/front/5/f/5f92bfac-c4ad-4155-855e-cf330f2c8145.jpg?1593452678" style="border-radius: 15px" width="342" height="auto">
    <img alt="Urza" src="https://c1.scryfall.com/file/scryfall-cards/large/front/a/b/abcef9d1-7f5d-4fe8-b17d-f97c1b30f4a7.jpg?1562385503" style="border-radius: 15px" width="342" height="auto">
    <img alt="Volrath" src="https://c1.scryfall.com/file/scryfall-cards/large/front/c/c/ccf13a39-55cb-4e13-8cf1-e99999176429.jpg?1562385543" style="border-radius: 15px" width="342" height="auto">
    <img alt="Xantcha" src="https://c1.scryfall.com/file/scryfall-cards/large/front/f/0/f0c696d1-0b5d-4102-ad93-46cb8e1e5d91.jpg?1562385559" style="border-radius: 15px" width="342" height="auto">
</details>
<details>
    <summary> Vanguards (table) </summary>
    
| Name                      | Effect (Errata'd)                                                                                                                        | Hand Mod | Health Mod |
|---------------------------|------------------------------------------------------------------------------------------------------------------------------------------|:--------:|:----------:|
| Ashnod                    | Whenever a creature deals damage to you, destroy it.                                                                                     | +1       | -8         |
| Barrin                    | Sacrifice a permanent: Return target creature to its owner’s hand.                                                                       | 0        | +6         |
| Crovax                    | Whenever a creature you control deals damage to a creature or player, you gain 1 life.                                                   | +2       | 0          |
| Eladamri                  | {0}: The next 1 damage that would be dealt to target creature you control is dealt to you instead.                                       | -1       | +15        |
| Ertai                     | Creatures you control have hexproof.                                                                                                     | -1       | +4         |
| Gerrard                   | At the beginning of your draw step, draw an additional card.                                                                             | -4       | 0          |
| Gix                       | {3}: Return target creature card from your graveyard to your hand.                                                                       | -2       | +18        |
| Greven il-Vec             | Whenever a creature you control deals damage to a creature, destroy the other creature. It can’t be regenerated.                         | -1       | +2         |
| Hanna                     | Spells you cast cost {1} less to cast.                                                                                                   | +1       | -5         |
| Karn                      | Each noncreature artifact you control is an artifact creature with power and toughness each equal to its mana value.                     | +1       | +6         |
| Lyna                      | Creatures you control have shadow. (They can block and be blocked only by creatures with shadow.)                                        | +2       | -4         |
| Maraxus                   | Creatures you control get +1/+0.                                                                                                         | +1       | +2         |
| Mirri                     | If a basic land you control is tapped for mana, it produces mana of a color of your choice instead of any other type.                    | 0        | +5         |
| Mishra                    | If a creature you control would deal damage, it deals double that damage instead.                                                        | 0        | -3         |
| Multani                   | Creatures you control get +X/+0, where X is the number of cards in your hand.                                                            | -3       | -2         |
| Oracle                    | {0}: Untap target attacking creature you control and remove it from combat.                                                              | +1       | +9         |
| Orim                      | Creatures you control have reach.                                                                                                        | 0        | +12        |
| Rofellos                  | Whenever a creature you control dies, draw a card.                                                                                       | -2       | +4         |
| Selenia                   | Creatures you control have vigilance.                                                                                                    | +1       | +7         |
| Serra                     | Creatures you control get +0/+2.                                                                                                         | +1       | +1         |
| Sidar Kondo               | {3}: Target creature gets +3/+3 until end of turn.                                                                                       | -1       | +12        |
| Sisay                     | Whenever you tap a land for mana, add one mana of any type that land produced.                                                           | -2       | -3         |
| Sliver Queen Brood Mother | {3}: Create a 1/1 colorless Sliver creature token.                                                                                       | 0        | +8         |
| Squee                     | Your opponents play with their hands revealed.                                                                                           | +3       | -4         |
| Starke                    | At the beginning of your draw step, you may draw an additional card. If you do, put a card from your hand on the bottom of your library. | 0        | -2         |
| Tahngarth                 | Creatures you control have haste.                                                                                                        | -1       | +7         |
| Takara                    | Sacrifice a creature: Takara deals 1 damage to any target.                                                                               | +3       | -8         |
| Tawnos                    | You may cast artifact, creature, and enchantment spells as though they had flash.                                                        | +3       | -4         |
| Titania                   | You may play an additional land on each of your turns.                                                                                   | +2       | -5         |
| Urza                      | {3}: Urza deals 1 damage to any target.                                                                                                  | -1       | +10        |
| Volrath                   | Whenever a creature you control is put into your graveyard from the battlefield, you may put it on top of your library.                  | +2       | -3         |
| Xantcha                   | Sacrifice a permanent: Regenerate target creature.                                                                                       | +1       | +3         |

</details>

# Dungeons
I haven't really seen these yet so I dunno what the deal is.
Wanted to list the possibility here because it seems cool.
Because most players probably won't be using cards that take advantage of this it'll need to be changed.
<details>
    <summary>Dungeons (Images)</summary>
    <img alt="Lost Mine of Phandelver" src="https://c1.scryfall.com/file/scryfall-cards/large/front/5/9/59b11ff8-f118-4978-87dd-509dc0c8c932.jpg?1626574309" style="border-radius: 15px" width="342" height="auto">    
    <img alt="Dungeon of the Mad Mage" src="https://c1.scryfall.com/file/scryfall-cards/large/front/6/f/6f509dbe-6ec7-4438-ab36-e20be46c9922.jpg?1626574247" style="border-radius: 15px" width="342" height="auto">
    <img alt="Tomb of Annihilation" src="https://c1.scryfall.com/file/scryfall-cards/large/front/7/0/70b284bd-7a8f-4b60-8238-f746bdc5b236.jpg?1626574335" style="border-radius: 15px" width="342" height="auto">

</details>

# Curses 
Made untargetable but still affect the player.
Enchants them at the start of the game

<details>
    <summary> Curses (Images) </summary>
    <img alt="Curse of Bloodletting" src="https://c1.scryfall.com/file/scryfall-cards/large/front/9/d/9dc4ac6f-0005-47f8-bee9-10429cc542e4.jpg?1562932270" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Death's Hold" src="https://c1.scryfall.com/file/scryfall-cards/large/front/1/7/1774d0a8-1cd3-4582-ace0-1caff92af0e7.jpg?1562826771" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Disturbance" src="https://c1.scryfall.com/file/scryfall-cards/large/front/8/0/80d42b98-62d2-4ca6-8478-36222d709048.jpg?1625976760" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Exhaustion" src="https://c1.scryfall.com/file/scryfall-cards/large/front/b/7/b737a959-e974-4b2a-8dca-a257da6084b0.jpg?1562938194" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Fool's Wisdom" src="https://c1.scryfall.com/file/scryfall-cards/large/front/9/b/9b77ded4-a8af-4065-8c4a-fd76e7cdcc59.jpg?1568003475" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Oblivion" src="https://c1.scryfall.com/file/scryfall-cards/large/front/c/1/c15cbd2f-8bbf-423a-81fe-521fd99bc8bf.jpg?1562836502" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Predation" src="https://c1.scryfall.com/file/scryfall-cards/large/front/d/a/daf631ad-762f-4bec-9588-aca88507cad5.jpg?1562854661" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Shallow Graves" src="https://c1.scryfall.com/file/scryfall-cards/large/front/8/4/849996e2-fb62-428f-88f1-4ac419617395.jpg?1562922975" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Stalked Prey" src="https://c1.scryfall.com/file/scryfall-cards/large/front/1/1/11a18883-8990-40a0-bcb2-e01d0e82bfad.jpg?1562826328" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of the Bloody Tome" src="https://c1.scryfall.com/file/scryfall-cards/large/front/c/7/c7865e11-263b-4d61-af54-907c1acbb54f.jpg?1562836859" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of the Pierced Heart" src="https://c1.scryfall.com/file/scryfall-cards/large/front/7/1/71010182-c004-4d18-adab-80319cd1e625.jpg?1562831956" style="border-radius: 15px" width="342" height="auto">
    <img alt="Curse of Thirst" src="https://c1.scryfall.com/file/scryfall-cards/large/front/a/2/a23ed5d1-44dc-4733-9e01-65fbc5dc02f2.jpg?1562933294" style="border-radius: 15px" width="342" height="auto">
</details>

<details>
    <summary>Curses (Table)</summary>
    
| Curse                      | Effect                                                                                                                                           | CMC | Rarity |
|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|:---:|:------:|
| Curse of Bloodletting      | If a source would deal damage to enchanted player, it deals double that damage to that player instead.                                           | 5   | 3      |
| Curse of Death's Hold      | Creatures enchanted player controls get -1/-1.                                                                                                   | 5   | 3      |
| Curse of Disturbance       | Whenever enchanted player is attacked, create a 2/2 black Zombie creature token. Each opponent attacking that player does the same.              | 3   | 1      |
| Curse of Exhaustion        | Enchanted player can't cast more than one spell each turn.                                                                                       | 4   | 1      |
| Curse of Fool's Wisdom     | Whenever enchanted player draws a card, they lose 2 life and you gain 2 life.                                                                    | 6   | 3      |
| Curse of Oblivion          | At the beginning of enchanted player's upkeep, that player exiles two cards from their graveyard.                                                | 4   | 0      |
| Curse of Predation         | Whenever a creature attacks enchanted player, put a +1/+1 counter on it.                                                                         | 3   | 2      |
| Curse of Shallow Graves    | Whenever a player attacks enchanted player with one or more creatures, the attacking player may create a tapped 2/2 black Zombie creature token. | 3   | 1      |
| Curse of Stalked Prey      | Whenever a creature deals combat damage to enchanted player, put a +1/+1 counter on that creature.                                               | 2   | 3      |
| Curse of the Bloody Tome   | At the beginning of enchanted player's upkeep, that player mills two cards.                                                                      | 3   | 0      |
| Curse of the Pierced Heart | At the beginning of enchanted player's upkeep, Curse of the Pierced Heart deals 1 damage to that player or a planeswalker that player controls.  | 2   | 0      |
| Curse of Thirst            | At the beginning of enchanted player's upkeep, Curse of Thirst deals damage to that player equal to the number of Curses attached to them.       | 5   | 1      |

</details>
